/////////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello1.cpp
/// @version 1
/// 
/// @author Byron Soriano <@byrongs@hawaii.edu>
/// @date   Feb 28 2022
////////////////////////////////////////////////////////////////////////////////

#include<iostream>

using namespace std;


int main() {
	cout << "Hello world" << endl;
	return 0;
}
